# Cicada Pattern #

Cicada pattern pseudo random number generator.

### About ###

* Magar Dmytro 25.07.2015
* Original idea: Alex Walker & Cicadas. [Cicada Principle](http://www.sitepoint.com/the-cicada-principle-and-why-it-matters-to-web-designers/)
* There is no double reps in output sequence, so it's convenient for gamedev.
* Also it has fast method to get an array of prime numbers.

### Structure ###

* CicadaPattern.cs - Cicada pattern class.
* Program.cs - Example of use.