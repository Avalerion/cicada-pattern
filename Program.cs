﻿using System;
/// <summary>
/// Cicada pattern example.
/// </summary>
class Program {
	public static void Main(string[] args) {
		Console.WriteLine("Pseudo random numbers without double reps!");
		CicadaPattern pattern = new CicadaPattern(10, DateTime.Now.Millisecond);
		for (int i = 0; i < 80 * 22; i++)
			Console.Write(pattern.Get(i));
		Console.WriteLine("Press any key to exit...");
		Console.ReadKey(true);
	}
}