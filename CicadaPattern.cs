﻿using System;
/// <summary>
/// CicadaPattern
/// Dmytro Magar 25.07.2015
/// Original idea: Alex Walker & Cicadas.
/// About principle:
/// http://www.sitepoint.com/the-cicada-principle-and-why-it-matters-to-web-designers/
/// </summary>
public class CicadaPattern {
	#region DATA
	private uint[] table;
	private int length;
	private int offset;
	#endregion DATA
	#region CONSTRUCTOR
	/// <summary>
	/// Create pattern generator with limited range. 
	/// </summary>
	/// <param name="length">Range of output indices</param>
	/// <param name="offset">Randomization offset</param>
	public CicadaPattern(int length, int offset = 0) {
		if (length < 3) throw new ArgumentOutOfRangeException();
		table = GetPrimeNumbers((uint)length - 1);
		this.length = length - 2;
		this.offset = offset != 0 ? offset : (int)table[this.length];
	}
	#endregion CONSTRUCTOR
	#region METHODS
	public int this[int i] {
		get { return Get(i); }
	}
	/// <summary>
	/// Get index at random position.
	/// </summary>
	/// <param name="position">Some position</param>
	/// <returns>Random index</returns>
	public int Get(int position) {
		int num = position + offset;
		for (int i = length; i >= 0; i--)
			if(num % table[i] == 0)
				return i + 1;
		return 0;
	}
	#endregion METHODS
	#region FUNCTIONS
	/// <summary>
	/// Create prime numbers array.
	/// </summary>
	/// <param name="length">Length of array</param>
	/// <returns>Prime numbers array</returns>
	private static uint[] GetPrimeNumbers(uint length) {
		if (length < 1) throw new ArgumentOutOfRangeException();
		uint a = 3, b = 1, c = 0;
		uint[] list = new uint[length];
		list[0] = 2;
		do {
			if(c == b - 1) list[b++] = a;
			else if(a % list[c] != 0) { c++; continue; }
			do a += 2; while(a % 10 == 5 && a > 10);
			c = 0;
		} while (b < length);
		return list;
	}
	#endregion FUNCTIONS
}